# Bets TicTacToe
> Projeto Integrado Modelagem e Simulação

Just a study replication.

## Project objective
Apply the knowledge acquired during the ***Modelling and Simulation*** classes, in addition to extra research to produce:
- A case study about the application of Modelling and Simulation in the format of a case study or article or;
- A software for solving a problem which involves Modelling and Simulation.

## Deployment
No deployment instructions available for this project ATM.

## Contributing
The project is **NOT** open to contributions.

## Versioning
I use [GIT](https://git-scm.com/) for versioning.

## Author
 * [Roberto Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/) - December 2021

## Disclaimer
References and citations are present in the *Simulation.ipynb* file.
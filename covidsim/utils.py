from matplotlib import pyplot as plt


def plot_path(path,
              label,
              times,
              title="",
              x_label="",
              y_label="",
              legend_loc="upper left"):
    fig, ax = plt.subplots()

    fig.set_facecolor("white")

    ax.set_title(title, fontsize=20)
    ax.set_xlabel(x_label, fontsize=18)
    ax.set_ylabel(y_label, fontsize=18)

    ax.plot(times, path, label=label)

    ax.legend(loc=legend_loc)

    plt.show()


def plot_paths(paths,
               labels,
               times,
               title="",
               x_label="",
               y_label="",
               legend_loc="upper left"):
    fig, ax = plt.subplots()

    fig.set_facecolor("white")

    ax.set_title(title, fontsize=20)
    ax.set_xlabel(x_label, fontsize=18)
    ax.set_ylabel(y_label, fontsize=18)

    for path, label in zip(paths, labels):
        ax.plot(times, path, label=label)

    ax.legend(loc=legend_loc)

    plt.show()

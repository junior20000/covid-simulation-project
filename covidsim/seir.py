import numpy as np
from scipy.integrate import odeint


def derivative(susceptible: float,
               exposed: float,
               infected: float,
               incubation_period: float,
               illness_duration: float,
               meeting_rate: float) -> tuple:
    bt = meeting_rate * illness_duration
    ds = -bt * (susceptible / 1) * infected
    de = ((bt * (susceptible / 1)) * infected) - (incubation_period * exposed)
    di = (incubation_period * exposed) - (infected * illness_duration)
    dr = infected * illness_duration

    return ds, de, di, dr


def rit(r0, ravg, mitigation, time):
    return r0 * np.exp(-mitigation * time) + (1 - np.exp(-mitigation * time)) * ravg


def drit(rit_, ravg, mitigation):
    return -mitigation * (rit_ - ravg)


def rt(r1t, r2t):
    return (r1t + r2t) / 2


def rt2(r1t, r1avg, r2t, r2avg, mitigation1, mitigation2, time):
    r1t = rit(r1t, r1avg, mitigation1, time)
    r2t = rit(r2t, r2avg, mitigation2, time)

    return rt(r1t, r2t)


def drt(dr1t, dr2t):
    return dr1t - dr2t


def drt2(r1t, r1avg, r2t, r2avg, mitigation1, mitigation2, time):
    r1t = rit(r1t, r1avg, mitigation1, time)
    r2t = rit(r2t, r2avg, mitigation2, time)

    return -0.5 * mitigation1 * (r1t - r1avg) - 0.5 * mitigation2 * (r2t - r2avg)


def seir_integrate(incubation_period, illness_duration, x_0, time, r1t, r1avg, r2t, r2avg,
                   mitigation1=1, mitigation2=1):
    s, e, i, _ = x_0

    r1t = rit(r1t, r1avg, mitigation1, time)
    r2t = rit(r2t, r2avg, mitigation2, time)
    rt_ = rt(r1t, r2t)

    return derivative(s, e, i, incubation_period, illness_duration, rt_)


def solve_path(incubation_period: float, illness_duration: float, x_0, t_vec, r1t, r1avg, r2t, r2avg,
               mitigation1=1, mitigation2=1) -> tuple:
    """Solves the path in relation to the time vector.
    Gets all the points along the model curve based on the time vector.

    Parameters
    ----------
    incubation_period : float
        Time until an individual can become a transmitter.
    illness_duration : float
        Period of time that an individual can act as a transmitter before incubation.
    t_vec : collection of floats
        All the points to retrieve from the curve.
    x_0 : Collection
        State vector (s, e, i).

    Returns
    -------
    tuple
        Returns the (i)nfected path and (c)umulative cases path.

    """

    seir_der = lambda x, t: seir_integrate(incubation_period, illness_duration, x, t, r1t, r1avg, r2t, r2avg,
                                           mitigation1, mitigation2)  # TODO: replace lambda with proper function.

    s_path, e_path, i_path, _ = odeint(seir_der, x_0, t_vec).transpose()

    c_path = 1 - s_path - e_path  # cumulative cases
    return i_path, c_path


def solve_rt_path(r1t, r1avg, r2t, r2avg, mitigation1, mitigation2, time_vec):
    return [rt2(r1t, r1avg, r2t, r2avg, mitigation1, mitigation2, time) for time in time_vec]

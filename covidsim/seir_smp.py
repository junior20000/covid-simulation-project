from typing import Callable, Collection, Union

from scipy.integrate import odeint


def derivative(x: Collection, incubation_period: float, illness_duration: float, t: float,
               r0: Union[float, Callable] = 1.6) -> tuple:
    """Time-based derivative of S, E, I (state) vector.

    Parameters
    ----------
    x : Collection
        State vector (s, e, i).
    incubation_period : float
        Time until an individual can become a transmitter.
    illness_duration : float
        Period of time that an individual can act as a transmitter before incubation.
    t : float
        Time.
    r0 : float or Callable, optional
        Transmission rate (defaulting to 1.6 due to representing about 50% reduction of the transmission rate).

    Returns
    -------
    tuple
        S, E, I derivative vector.

    """
    s, e, i = x

    # Updated exposed
    transmission_rate = r0(t) * illness_duration if callable(r0) else r0 * illness_duration
    ne = transmission_rate * s * i  # ne == (N)ew (E)xposed (value)

    # Time derivatives
    ds = - ne
    de = ne - incubation_period * e
    di = incubation_period * e - illness_duration * i

    return ds, de, di


def solve_path(r0: Union[float, Callable], incubation_period: float, illness_duration: float, t_vec: Collection[float],
               x_init: Collection[float]) -> tuple:
    """Solves the path in relation to the time vector.
    Gets all the points along the model curve based on the time vector.

    Parameters
    ----------
    r0 : float or Callable, optional
        Transmission rate (defaulting to 1.6 due to representing about 50% reduction of the transmission rate).
    incubation_period : float
        Time until an individual can become a transmitter.
    illness_duration : float
        Period of time that an individual can act as a transmitter before incubation.
    t_vec : collection of floats
        All the points to retrieve from the curve.
    x_init : Collection
        State vector (s, e, i).

    Returns
    -------
    tuple
        Returns the (i)nfected path and (c)umulative cases path.

    """
    sei_der = lambda x, t: derivative(x, incubation_period, illness_duration, t,
                                      r0)  # TODO: replace lambda with proper function.
    s_path, e_path, i_path = odeint(sei_der, x_init, t_vec).transpose()

    c_path = 1 - s_path - e_path  # cumulative cases
    return i_path, c_path
